
package libraryproject.mindvalley.com.mindvalley_prathyush_android_test.util;

import android.content.Context;
import android.util.DisplayMetrics;

import libraryproject.mindvalley.com.networkinglibrary.RequestQueue;
import libraryproject.mindvalley.com.networkinglibrary.cache.BitmapImageCache;
import libraryproject.mindvalley.com.networkinglibrary.cache.SimpleImageLoader;
import libraryproject.mindvalley.com.networkinglibrary.tools.NetworkTools;


/**
 * Helper class that is used to provide references to initialized RequestQueue(s) and ImageLoader(s)
 * 
 * @author Ognyan Bankov
 * 
 */
public class Utils {
    private static RequestQueue mRequestQueue;
    private static SimpleImageLoader mImageLoader;



    private Utils() {
        // no instances
    }

    public static void init(Context context) {
        mRequestQueue = NetworkTools.newRequestQueue(context);
        mImageLoader  = new SimpleImageLoader(mRequestQueue, BitmapImageCache.getInstance(null));

    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            throw new IllegalStateException("RequestQueue not initialized");
        }
    }

    /**
     * Returns instance of ImageLoader initialized with {@see FakeImageCache} which effectively means
     * that no memory caching is used. This is useful for images that you know that will be show
     * only once.
     * 
     * @return
     */
    public static SimpleImageLoader getImageLoader() {
        if (mImageLoader != null) {
            return mImageLoader;
        } else {
            throw new IllegalStateException("ImageLoader not initialized");
        }
    }

    public static int getInternetStrength(double speed)
    {
        //speed in kbps
        if(speed < 150)
        {
            //poor internet connection
            return 0;

        }else if(speed>=150 && speed < 550 )
        {
            //moderate internet connection
            return 1;
        }else if(speed>=550 && speed < 2000 )
        {
            //moderate internet connection
            return 2;
        }else if(speed>=2000 )
        {
            //moderate internet connection
            return 3;
        }

        return 0;


    }


}