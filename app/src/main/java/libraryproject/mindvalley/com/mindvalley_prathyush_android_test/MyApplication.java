package libraryproject.mindvalley.com.mindvalley_prathyush_android_test;

import android.app.Application;

import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.util.Utils;

/**
 * Created by prathyush on 01/08/16.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        init();
    }


    private void init() {
        Utils.init(this);
    }
}
