package libraryproject.mindvalley.com.mindvalley_prathyush_android_test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.R;
import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.activities.MainActivity;
import libraryproject.mindvalley.com.networkinglibrary.cache.SimpleImageLoader;
import libraryproject.mindvalley.com.networkinglibrary.ui.NetworkImageView;

/**
 * Created by prathyush on 02/08/16.
 */
public class GridAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> mItems;
    private SimpleImageLoader mImageLoader;



    private int imageQuality=0;

    public GridAdapter(Context mContext, ArrayList<String> mItems, SimpleImageLoader mImageLoader) {
        this.mContext     = mContext;
        this.mItems       = mItems;
        this.mImageLoader = mImageLoader;
    }

    public void setImageQuality(int imageQuality) {
        this.imageQuality = imageQuality;
        if(imageQuality != -1) {
            String quality = imageQuality >= 2 ? "High internet Connection" : imageQuality == 1 ? "Moderate internet Connection" : "Low internet Connection";
            String text = quality+".Calculated with respect to amount of data passed with time";
            Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        NetworkImageView mImageView;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.item_row, null);
        }

         mImageView     = (NetworkImageView) convertView.findViewById(R.id.iv_image);


         mImageView.setImageUrl(mItems.get(position),mImageLoader);

        mImageView.setDefaultImageResId(R.drawable.ic_add_a_photo_black_24dp);




        return convertView;
    }

    public void setList(ArrayList<String> list) {
        this.mItems = list;
        notifyDataSetChanged();
    }
}
