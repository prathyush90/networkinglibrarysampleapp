    package libraryproject.mindvalley.com.mindvalley_prathyush_android_test.activities;

    import android.Manifest;
    import android.content.pm.PackageManager;
    import android.os.Build;
    import android.support.v4.app.ActivityCompat;
    import android.support.v4.widget.SwipeRefreshLayout;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.util.DisplayMetrics;
    import android.util.Log;
    import android.view.View;
    import android.view.animation.Animation;
    import android.view.animation.AnimationSet;
    import android.view.animation.ScaleAnimation;
    import android.view.animation.TranslateAnimation;
    import android.widget.GridView;
    import android.widget.Toast;


    import org.json.JSONArray;
    import org.json.JSONException;


    import java.util.ArrayList;

    import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.R;
    import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.adapters.GridAdapter;
    import libraryproject.mindvalley.com.mindvalley_prathyush_android_test.util.Utils;
    import libraryproject.mindvalley.com.networkinglibrary.Request;
    import libraryproject.mindvalley.com.networkinglibrary.RequestQueue;
    import libraryproject.mindvalley.com.networkinglibrary.Response;
    import libraryproject.mindvalley.com.networkinglibrary.cache.SimpleImageLoader;
    import libraryproject.mindvalley.com.networkinglibrary.error.BaseError;
    import libraryproject.mindvalley.com.networkinglibrary.request.JsonArrayRequest;
    import libraryproject.mindvalley.com.networkinglibrary.ui.NetworkImageView;

    public class MainActivity extends AppCompatActivity {



        private GridView mGridView;
        private GridAdapter mAdapter;
        private SwipeRefreshLayout mSwipeToRefresh;
        private ArrayList<String> mItems = new ArrayList<>();
        private NetworkImageView fullScreenView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);
            mSwipeToRefresh  = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
            mGridView        = (GridView) findViewById(R.id.gridView);
            fullScreenView   = (NetworkImageView) findViewById(R.id.full_image);
            mAdapter         =  new GridAdapter(this,mItems,Utils.getImageLoader());
            mGridView.setAdapter(mAdapter);
            mSwipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    makeRequest();
                    Toast.makeText(MainActivity.this,"Refreshing the dashboard",Toast.LENGTH_LONG).show();
                }
            });


            CheckPermissionAndContinue();

        }

        public void CheckPermissionAndContinue() {

            if (isStoragePermissionGranted()) {
                 makeRequest();
            }
        }

        public void makeRequest()
        {
            RequestQueue queue = Utils.getRequestQueue();
            JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.GET,
                    " http://pastebin.com/raw/wgkJgazE",
                    null,
                    createMyReqSuccessListener(),
                    createMyReqErrorListener());
            queue.add(myReq);
        }




        public boolean isStoragePermissionGranted() {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    return false;
                }
            } else {
                return true;
            }


        }

        private Response.Listener<JSONArray> createMyReqSuccessListener() {
            return new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    System.out.println("xxxx");
                    if(mSwipeToRefresh.isRefreshing())
                    {
                        mSwipeToRefresh.setRefreshing(false);
                    }
                    for(int i=0;i<response.length();i++)
                    {
                        try {
                            mItems.add(response.getJSONObject(i).getJSONObject("user").getJSONObject("profile_image").getString("large"));
                            mItems.add(response.getJSONObject(i).getJSONObject("urls").getString("small"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.setList(mItems);

                        }
                    });

                }

                @Override
                public void internetSpeed(double speed) {
                    final int n = (speed !=0) ? Utils.getInternetStrength(speed): -1;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.setImageQuality(n);
                            }
                        });



                }


            };
        }


        private Response.ErrorListener createMyReqErrorListener() {
            return new Response.ErrorListener() {
                @Override
                public void onErrorResponse(BaseError error) {
                    Toast.makeText(MainActivity.this,"An error occured while making the api call",Toast.LENGTH_LONG).show();
                }
            };
        }



        @Override
        public void onRequestPermissionsResult(int requestCode,
                                               String permissions[], int[] grantResults) {
            switch (requestCode) {
                case 1: {

                    CheckPermissionAndContinue();
                    return;
                }
            }
        }


    }
