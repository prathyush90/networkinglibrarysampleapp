package libraryproject.mindvalley.com.networkinglibrary.helper;



import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import libraryproject.mindvalley.com.networkinglibrary.Response;

public class CountingOutputStream extends DataOutputStream {
    private final Response.ProgressListener progressListener;
    private long transferred;
    private long fileLength;

    public CountingOutputStream(final OutputStream out, long length,
                                final Response.ProgressListener listener) {
        super(out);
        fileLength = length;
        progressListener = listener;
        transferred = 0;
    }

    public void write(int b) throws IOException {
        out.write(b);
        if (progressListener != null) {
            transferred++;
            int prog = (int) (transferred * 100 / fileLength);
            progressListener.onProgress(transferred, prog);
        }
    }

}