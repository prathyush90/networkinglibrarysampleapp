

package libraryproject.mindvalley.com.networkinglibrary.tools;

import android.content.Context;
import android.net.http.AndroidHttpClient;



import java.io.File;

import libraryproject.mindvalley.com.networkinglibrary.Network;
import libraryproject.mindvalley.com.networkinglibrary.RequestQueue;
import libraryproject.mindvalley.com.networkinglibrary.cache.DiskBasedCache;
import libraryproject.mindvalley.com.networkinglibrary.helper.NetUtils;
import libraryproject.mindvalley.com.networkinglibrary.helper.Utils;

public class NetworkTools {

    private NetworkTools() {}

    /** Default on-disk cache directory. */
    private static final String DEFAULT_CACHE_DIR = "library";


    public static RequestQueue newRequestQueue(Context context, HttpStack stack) {
        File cacheDir = new File(context.getCacheDir(), DEFAULT_CACHE_DIR);
        if (stack == null) {
            stack =
                    new HttpClientStack(AndroidHttpClient.newInstance(
                            NetUtils.getUserAgent(context)));
        }

        Network network = new BasicNetwork(stack);

        RequestQueue queue = new RequestQueue(new DiskBasedCache(cacheDir), network);
        queue.start();

        return queue;
    }

    /**
     * Creates a default instance of the worker pool and calls {@link RequestQueue#start()} on it.
     *
     * @param context A {@link Context} to use for creating the cache dir.
     * @return A started {@link RequestQueue} instance.
     */
    public static RequestQueue newRequestQueue(Context context) {
        return newRequestQueue(context, null);
    }
}
