

package libraryproject.mindvalley.com.networkinglibrary.error;


import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Indicates that the server's response could not be parsed.
 */
@SuppressWarnings("serial")
public class ParseError extends BaseError {
    public ParseError() { }

    public ParseError(NetworkResponse networkResponse) {
        super(networkResponse);
    }
    
    public ParseError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public ParseError(Throwable cause) {
        super(cause);
    }
}
