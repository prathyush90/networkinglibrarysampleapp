

package libraryproject.mindvalley.com.networkinglibrary.error;



/**
 * Exception style class encapsulating Volley errors
 */

import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Exception style class encapsulating Volley errors
 */
@SuppressWarnings("serial")
public class BaseError extends Exception {
    public final NetworkResponse networkResponse;
    private long networkTimeMs;
    public BaseError() {
        networkResponse = null;
    }
    public BaseError(NetworkResponse response) {
        networkResponse = response;
    }
    public BaseError(String exceptionMessage) {
        super(exceptionMessage);
        networkResponse = null;
    }
    public BaseError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
        networkResponse = null;
    }
    public BaseError(Throwable cause) {
        super(cause);
        networkResponse = null;
    }
    public BaseError(NetworkResponse response, Throwable cause) {
        super(cause);
        networkResponse = response;
    }
    public void setNetworkTimeMs(long networkTimeMs) {
        this.networkTimeMs = networkTimeMs;
    }
    public long getNetworkTimeMs() {
        return networkTimeMs;
    }
}
