

package libraryproject.mindvalley.com.networkinglibrary.error;


import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Indicates that the server responded with an error response.
 */
@SuppressWarnings("serial")
public class ServerError extends BaseError {
    public ServerError(NetworkResponse networkResponse) {
        super(networkResponse);
    }

    public ServerError() {
        super();
    }
}
