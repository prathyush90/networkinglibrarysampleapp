

package libraryproject.mindvalley.com.networkinglibrary;



import libraryproject.mindvalley.com.networkinglibrary.error.BaseError;

/**
 * Retry policy for a request.
 */
public interface RetryPolicy {

    /**
     * Returns the current timeout (used for logging).
     */
    public int getCurrentTimeout();

    /**
     * Returns the current retry count (used for logging).
     */
    public int getCurrentRetryCount();


    public void retry(BaseError error) throws BaseError;
}
