/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package libraryproject.mindvalley.com.networkinglibrary.request;



import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;
import libraryproject.mindvalley.com.networkinglibrary.Request;
import libraryproject.mindvalley.com.networkinglibrary.Response;
import libraryproject.mindvalley.com.networkinglibrary.error.ParseError;
import libraryproject.mindvalley.com.networkinglibrary.tools.HttpHeaderParser;

/**
 * A canned request for retrieving the response body at a given URL as a String.
 */
public class XmlRequest extends Request<String> {
    private final Response.Listener<Document> mListener;

    /**
     * Creates a new request with the given method.
     *
     * @param method the request {@link Method} to use
     * @param url URL to fetch the string at
     * @param listener Listener to receive the String response
     * @param errorListener Error listener, or null to ignore errors
     */
    public XmlRequest(int method, String url, Response.Listener<Document> listener,
                      Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mListener = listener;
    }

    /**
     * Creates a new GET request.
     *
     * @param url URL to fetch the string at
     * @param listener Listener to receive the String response
     * @param errorListener Error listener, or null to ignore errors
     */
    public XmlRequest(String url, Response.Listener<Document> listener, Response.ErrorListener errorListener) {
        this(Method.GET, url, listener, errorListener);
    }

    @Override
    protected void deliverResponse(String response) {
    	if(null != mListener){
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            try
            {
                builder = factory.newDocumentBuilder();
                Document document = builder.parse( new InputSource( new StringReader((String) response) ));
                mListener.onResponse(document);
            } catch (Exception e) {
                e.printStackTrace();
                mListener.onResponse(null);
            }
    	}
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = null;
        try {

            Document doc = null;

            doc = builder.parse(new ByteArrayInputStream(response.data));
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = null;
            transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return Response.success(
                    writer.toString(), HttpHeaderParser.parseCacheHeaders(response));

        } catch (SAXException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (IOException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (TransformerException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }
}
