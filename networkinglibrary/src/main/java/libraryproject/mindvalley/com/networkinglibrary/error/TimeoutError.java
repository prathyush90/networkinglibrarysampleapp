

package libraryproject.mindvalley.com.networkinglibrary.error;


import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Indicates that the connection or the socket timed out.
 */
@SuppressWarnings("serial")
public class TimeoutError extends BaseError {
	
	public TimeoutError() {
		super(new NetworkResponse(-1, null, null, false));
	}
}
