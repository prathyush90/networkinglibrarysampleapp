

package libraryproject.mindvalley.com.networkinglibrary;


import libraryproject.mindvalley.com.networkinglibrary.error.BaseError;

/**
 * An interface for performing requests.
 */
public interface Network {

    public NetworkResponse performRequest(Request<?> request) throws BaseError;
}
