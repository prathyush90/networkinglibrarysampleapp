

package libraryproject.mindvalley.com.networkinglibrary.error;


import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Error indicating that no connection could be established when performing a Volley request.
 */
@SuppressWarnings("serial")
public class NoConnectionError extends NetworkError {
    public NoConnectionError() {
        super();
    }

    public NoConnectionError(NetworkResponse networkResponse) {
        super(networkResponse);
    }

    public NoConnectionError(NetworkResponse networkResponse, Throwable reason) {
        super(networkResponse, reason);
    }

    public NoConnectionError(Throwable reason) {
        super(reason);
    }
}
