

package libraryproject.mindvalley.com.networkinglibrary.error;


import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;

/**
 * Indicates that there was a network error when performing a Volley request.
 */
@SuppressWarnings("serial")
public class NetworkError extends BaseError {
    public NetworkError() {
        super();
    }

    public NetworkError(Throwable cause) {
        super(cause);
    }

    public NetworkError(NetworkResponse networkResponse) {
        super(networkResponse);
    }

    public NetworkError(NetworkResponse networkResponse, Throwable reason) {
        super(networkResponse, reason);
    }
}
