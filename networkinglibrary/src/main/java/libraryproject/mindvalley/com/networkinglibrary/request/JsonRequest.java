

package libraryproject.mindvalley.com.networkinglibrary.request;



import java.io.UnsupportedEncodingException;

import libraryproject.mindvalley.com.networkinglibrary.NetworkResponse;
import libraryproject.mindvalley.com.networkinglibrary.Request;
import libraryproject.mindvalley.com.networkinglibrary.Response;

/**
 * A request for retrieving a T type response body at a given URL that also
 * optionally sends along a JSON body in the request specified.
 *
 * @param <T> JSON type of response expected
 */
public abstract class JsonRequest<T> extends Request<T> {
    /** Charset for request. */
    protected static final String PROTOCOL_CHARSET = "utf-8";

    /** Content type for request. */
    private static final String PROTOCOL_CONTENT_TYPE =
        String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private final Response.Listener<T> mListener;
    private final String mRequestBody;



    private long millisecondsStart = 0l;



    private double speed;

    /**
     * Deprecated constructor for a JsonRequest which defaults to GET unless {@link #getPostBody()}
     * or {@link #getPostParams()} is overridden (which defaults to POST).
     *
     *
     */
    public JsonRequest(String url, String requestBody, Response.Listener<T> listener,
            Response.ErrorListener errorListener) {
        this(Method.DEPRECATED_GET_OR_POST, url, requestBody, listener, errorListener);
    }

    public JsonRequest(int method, String url, String requestBody, Response.Listener<T> listener,
            Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mListener = listener;
        mRequestBody = requestBody;
        millisecondsStart = System.currentTimeMillis();
    }

    @Override
    protected void deliverResponse(T response) {
    	if(null != mListener){
    		mListener.onResponse(response);

    	}
    }

    public long getMillisecondsStart() {
        return millisecondsStart;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
        if(null != mListener){
            mListener.internetSpeed(speed);

        }
    }



    @Override
    abstract protected Response<T> parseNetworkResponse(NetworkResponse response);

    /**
     * @deprecated Use {@link #getBodyContentType()}.
     */
    @Override
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    /**
     * @deprecated Use {@link #getBody()}.
     */
    @Override
    public byte[] getPostBody() {
        return getBody();
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {

            return null;
        }
    }
}
